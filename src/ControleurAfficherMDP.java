import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurAfficherMDP implements EventHandler<ActionEvent> {
    private VueVAE vue;
    public ControleurAfficherMDP(VueVAE vue) {
        this.vue = vue;
    }
    @Override
    public void handle(ActionEvent event) {
        this.vue.popUpMessageVoirMDP().showAndWait();
    }
}
