import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControlleurCreerCompte implements EventHandler<ActionEvent> {
    /**
     * La vue de l'application
     */
    private VueVAE vue;

    /**
     * permet d'accéder à une fenêtre permettant de s'inscrire dans l'application, de créer son compte
     * @param vue La vue de l'application
     */
    public ControlleurCreerCompte(VueVAE vue) {
        this.vue = vue;
    }

    /**
     * on change la fenêtre actuelle en celle que la fonction appellé utilise, la fenêtre d'incription à l'application
     * @param a lorsque le bouton est pressé
     */
    @Override
    public void handle(ActionEvent a) {
        try {
            this.vue.modeInscription();
        } catch (Exception e) {
            System.out.println("Connexion impossible");
        }
    }
}
