import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurFileChooser implements EventHandler<ActionEvent> {

    /**
     * La vue de l'application
     */
    private VueVAE vue;

    /**
     * va permettre lorsque l'on cré une nouvelle annonce de choisir une image dans le périphérique de l'utilisateur
     * @param vue La vue de l'application
     */
    public ControleurFileChooser(VueVAE vue) {
        this.vue = vue;
    }

    /**
     * appelle une fonction qui va permettre à l'utilisateur de choisir une image de son répertoire
     * @param actionEvent si le bouton est pressé
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        this.vue.choixImage();
    }
    
}
