public class Utilisateur {
    private int idUt;
    private String pseudoUt;
    private String emailUt;
    private String mdpUt;
    private boolean active;
    private int nbAnnonces;
    private int nbVente;
    private int role;

    public Utilisateur(int idUt, String pseudoUt, String emailUt, String mdpUt, boolean active) {
        this.idUt = idUt;
        this.pseudoUt = pseudoUt;
        this.emailUt = emailUt;
        this.mdpUt = mdpUt;
        this.active = active;
        this.nbAnnonces = 0;
        this.nbVente = 0;
        this.role = 2;
    }
    public void addAnnonce() {
        this.nbAnnonces += 1;
    }

    public void setRole(int y){
        this.role = y;
    }

    public int getRole(){
        return this.role;
    }

    public void addVente() {
        this.nbVente += 1;
    }
    public int getIdUt() {
        return this.idUt;
    }
    public String getPseudoUt() {
        return this.pseudoUt;
    }
    public String getEmailUt() {
        return this.emailUt;
    }
    public String getMdpUt() {
        return this.mdpUt;
    }
    public boolean isActive() {
        return this.active;
    }
    public void setPseudo(String pseudo) {
        this.pseudoUt = pseudo;
    }

    public void setEmail(String email) {
        this.emailUt = email;
    }

    public void setMdp(String mdp) {
        this.mdpUt = mdp;
    }

    public void setActif(boolean actif) {
        this.active = actif;
    }
    public int getNbAnnonces() {
        return this.nbAnnonces;
    }
    public int getNbVente() {
        return this.nbVente;
    }
}
