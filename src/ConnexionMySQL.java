import java.sql.*;

public class ConnexionMySQL {
	private Connection mysql=null;
	private boolean connecte=false;
	public ConnexionMySQL() throws ClassNotFoundException{
		// try{
		// 	Class.forName("org.mariadb.jdbc.Driver");
		// }
		// catch(SQLException e) {
		// 	System.out.println("erreur");
		// }
	}

	public void connecter(String nomBase, String nomLogin, String motDePasse) throws SQLException{
		// si tout c'est bien passé la connexion n'est plus nulle
		
		Connection c;
		try{
			Class.forName("org.mariadb.jdbc.Driver");
			c = DriverManager.getConnection("jdbc:mysql://servinfo-mariadb:3306/"+nomBase,nomLogin,motDePasse);
			this.mysql = c;
			this.connecte = true;
		}
		catch(ClassNotFoundException e) {
			System.out.println("Erreur");
		}
		this.connecte=this.mysql!=null;
	}
	public void close() throws SQLException {
		// fermer la connexion
		this.connecte=false;
	}

    	public boolean isConnecte() { return this.connecte;}
	public Statement createStatement() throws SQLException {
		return this.mysql.createStatement();
	}

	public PreparedStatement prepareStatement(String requete) throws SQLException{
		return this.mysql.prepareStatement(requete);
	}
	
}
