import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurDeconnexion implements EventHandler<ActionEvent> {

    private VueVAE appli;

    public ControleurDeconnexion(VueVAE appli) {
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.appli.modeConnexion();
    }
}


