import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.RadioButton;

public class ControleurBoutonCate implements EventHandler<ActionEvent> {

    private VueVAE appli;

    public ControleurBoutonCate(VueVAE appli) {
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        RadioButton btn = (RadioButton) actionEvent.getTarget();
        this.appli.setCateObjetAVendre(btn.getText());
    }
}