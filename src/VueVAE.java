import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.text.*;
import javafx.scene.chart.PieChart;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.geometry.Side;
import java.util.List;
import javafx.scene.control.PasswordField;
import java.util.ArrayList;
import javafx.scene.control.TextArea;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Hyperlink;
import javafx.scene.shape.Rectangle;
import java.sql.SQLException;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import java.io.File;
import java.util.HashMap;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.text.Text;
import java.util.regex.Pattern;
import java.sql.*;

public class VueVAE extends Application{


    /** Panel principale qui permet d'afficher l'apllication et changer de page */
    private BorderPane panelPrincipale;

    // Attributs pour la page de vente  : 

    /** Permet de récupérer le nom de l'objet à mettre en vente  */
    private TextField nomObjetAVendre;

    /** Permet de récupérer la description de l'objet à mettre en vente */
    private TextArea  descriptionObjetAVendre;

    /** Permet de récupérer le prix de départ de l'objet à mettre en vente */
    private TextField prixObjetAVendre;

    /** Permet de récupérer le prix minimum de l'objet à mettre en vente */
    private TextField prixMinObjetAVendre;

    /** Permet de récupérer le temps avant la mise en vente de l'objet  */
    private TextField tempsAvantMiseEnVente;

    /** Permet de récupérer la durée de vente de l'objet à mettre en vente  */
    private TextField DureeDeVente;
    
    /** Image de l'objet à vendre, peut être modifié grâce au FileChooser */
    private ImageView ImageObjetAVendre;

    /** La liste des annonce que l'utilisateur connecté à mis en vente */
    private List<Annonce> lesAnnonces;

    /** La catégorie de l'objet à mettre en vente */
    private String cateObjetAVendre;

    /** Le FileChooser qui permet de séléctionner l'image pour l'objet à vendre */
    private FileChooser fc;

    /** La scene qui correspond à l'application */
    private Scene scene;

    //Fin attributs de la page Vente


    /** bouton de connexion */
    private Button bConnexion;

    /** bouton "pas encore membre" */
    private TextFlow lSignUp;

    /** Zone de texte addresse mail */
    private TextField tMail;

    /** Zone de texte mot de passe */
    private PasswordField tPassword;

    private Pane pagededroite;
    private Pane pagedegauche;
    private ConnexionMySQL connexionMySQL;
    private EncherirBD encherirBD;

    private Utilisateur user;

    private Pane contenu;

    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$");

    public TextField pseudo;
    public TextField email;
    public PasswordField mdp1;
    public PasswordField mdp2;
    public Button creation;


    private GridPane gridPane;
    private Button menubouton;
    private TextField recherche;

    private List<Produit> produits;
    private HashMap<Produit, Button> boutons;
    private HashMap<Produit, TextField> champs;

    private TextField nouveauPseudo;
    private TextField nouvelEmail;
    private PasswordField nouveauMDP;
    
    @Override
    public void init() {
        this.panelPrincipale = new BorderPane();

        // Initialisation page vente
        
        this.nomObjetAVendre = new TextField();
        this.descriptionObjetAVendre = new TextArea();
        this.prixObjetAVendre = new TextField();
        this.prixMinObjetAVendre = new TextField();
        this.tempsAvantMiseEnVente = new TextField();
        this.DureeDeVente =new TextField();
        this.lesAnnonces = new ArrayList<>();
        this.cateObjetAVendre = "Meuble";
        this.ImageObjetAVendre = new ImageView("imgBase.png");
        // Valeur test
        // Image img = new Image("ImgDrac.jpg");
        // this.lesAnnonces.add(new Annonce("Une carte", "elle est PSA 10", img, 4584));
        // this.lesAnnonces.add(new Annonce("Une carte", "elle est PSA 10", img, 4584));
        // this.lesAnnonces.add(new Annonce("Une carte", "elle est PSA 10", img, 4584));
        // this.lesAnnonces.add(new Annonce("Une carte", "elle est PSA 10", img, 4584));
        // this.lesAnnonces.add(new Annonce("Une carte", "elle est PSA 10", img, 4584));
        //Fin initialisation page vente

        this.pseudo = new TextField();
        this.email = new TextField();
        this.mdp1 = new PasswordField();
        this.mdp2 = new PasswordField();
        this.creation = new Button();
        this.creation.setOnAction(new ControleurDonne(this));

        

        // Création du bouton de connexion
        this.bConnexion = new Button("Connexion");
        // Création du lien "pas encore membre"
        Label crea = new Label("Créer un compte");
        crea.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        Button createAccount = new Button();
        createAccount.setGraphic(crea);
        createAccount.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: #00F5A2;");
        // Style du lien
        // Création du texte "Pas encore membre ?"
        lSignUp = new TextFlow(new Text("Pas encore membre ?    "), createAccount);
        // Action du lien
        createAccount.setOnAction(new ControlleurCreerCompte(this));
        // Création des zones de texte
        tMail = new TextField();
        tPassword = new PasswordField();
        // Style lien "pas encore membre"
        this.lSignUp.setTextAlignment(TextAlignment.RIGHT);
        this.lSignUp.setPadding(new Insets(20));
        this.lSignUp.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        // Style du bouton de connexion
        this.bConnexion.setPrefWidth(175);
        this.bConnexion.setPrefHeight(40);
        this.bConnexion.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: #00F5A2;");
        this.bConnexion.setOnAction(new ControleurBoutonConnexion(this));
        // Style de la zone de texte mail
        this.tMail.setPromptText("Adresse mail");
        this.tMail.setPrefWidth(200);
        this.tMail.setPrefHeight(50);
        // Style de la zone de texte mot de passe
        this.tPassword.setPromptText("Mot de passe");
        this.tPassword.setPrefWidth(200);
        this.tPassword.setPrefHeight(50);
        // Activation de la touche entrée
        this.tPassword.setOnKeyPressed(new EnterPressed(this));
        this.tMail.setOnKeyPressed(new EnterPressed(this));

        //Partie connexion Base de données

        try {
            this.connexionMySQL = new ConnexionMySQL();
        }
        catch (ClassNotFoundException ex){
            System.out.println("Driver MySQL non trouvé!!!");
            System.exit(1);
        }

        try {
            this.connexionMySQL.connecter("DBmechain", "mechain", "mechain");
            if (this.connexionMySQL.isConnecte()) {
                System.out.println("Connecté");
            }
            else {
                System.out.println("Pas connecté");
            }
        }
        catch (SQLException e) {
            System.out.println("Erreur");
        }

        this.encherirBD = new EncherirBD(this.connexionMySQL);
        this.user = null;
        // page produit
        this.produits = new ArrayList<Produit>();
        this.boutons = new HashMap<Produit, Button>();
        this.champs = new HashMap<Produit, TextField>();
        this.remplieProd();

        //haut de page 

        
    }

    @Override
    /**
     * Méthode qui permet de démarrer l'application
     * @param stage
     */
    public void start(Stage stage) {
        //Pane test = this.annonce("Dracaufeu holo PCA10", "ImgDrac.jpg", "Une carte magnifique", 12560);
        //Pane test = this.espaceVente();
        //Pane test = this.espaceArticlesEnVentes();
        Pane pagePrincipale = this.fenetreConnxeion();
        this.panelPrincipale.setCenter(pagePrincipale);

        Scene scene = new Scene(this.panelPrincipale,1200,800);
        stage.setResizable(false);
        stage.setTitle("VAE");
        stage.setScene(scene);
        this.scene = scene;
        stage.show();
    }

    /**
     * Méthode qui permet de créer un Pane représentant une annonce d'objet en vente 
     * @param nom String : le nom de l'objet 
     * @param imgA Image : Une image correspondant à l'objet
     * @param description String : La description de l'objet 
     * @param prix int : Le prix actuel de l'objet
     * @return Pane : Le pane qui représente l'annonce 
     */
    public Pane annonce(String nom, Image imgA, String description, int prix) {
        // Text du nom 
        Text txtNom = new Text(nom);
        txtNom.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        // Image
        ImageView img = new ImageView(imgA);
        img.setFitWidth(120);
        img.setFitHeight(140);
        Button b = new Button();
        b.setGraphic(img);
        // Text de la description
        Text txtDescription = new Text(description);
        txtDescription.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        txtDescription.setWrappingWidth(300);
        // Text du prix 
        Text txtPrix = new Text(prix+"€");
        txtPrix.setFont(Font.font("Arial", FontWeight.BOLD, 12));

        //Création des contenants 
        VBox vbPrincipale = new VBox();
        HBox hb = new HBox();
        VBox droite = new VBox();

        // Ajout des éléments dans les contenants 
        droite.getChildren().addAll(txtDescription,txtPrix);
        hb.getChildren().addAll(b,droite);
        vbPrincipale.getChildren().addAll(txtNom,hb);

        // Mise en forme 
        HBox.setMargin(b,new Insets(10,10,10,20));
        HBox.setMargin(droite,new Insets(10,0,0,10));
        VBox.setMargin(txtPrix,new Insets(100,0,0,20));

        return vbPrincipale;

    }

    /**
     * Méthode qui permet de créer le Pane représentant l'espace de vente 
     * @return Pane : le Pane de l'espce de vente
     */
    public Pane espaceVente() {
        Text titre = new Text("Mise en vente : ");
        titre.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        //Création Labels 
        Label l1 = new Label("Nom de l'objet : ");
        l1.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        Label l2  = new Label("Description de l'objet : ");
        l2.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        Label l3 = new Label("Prix de départ : ");
        l3.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        Label l4 = new Label("Image : ");
        l4.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        Label l5 = new Label("Prix minimum :");
        l5.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        Label l6 = new Label("Temps avant mise en vente : (en h)");
        l6.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        Label l7 = new Label("Durée de la vente : (en h)");
        l7.setFont(Font.font("Arial", FontWeight.BOLD, 17));

        //Création boutons
        Button bImage = new Button("Choisir une image");
        bImage.setOnAction(new ControleurFileChooser(this));
        Button bCreer = new Button("Ajouter l'annonce");
        bCreer.setOnAction(new ControleurCreerAnnonce(this));

        //Création RadioBoutons
        ToggleGroup tg = new ToggleGroup();
        RadioButton rb1 = new RadioButton("Vêtement");
        rb1.setToggleGroup(tg);
        rb1.setOnAction(new ControleurBoutonCate(this));
        RadioButton rb2 = new RadioButton("Ustensile Cuisine");
        rb2.setToggleGroup(tg);
        rb2.setOnAction(new ControleurBoutonCate(this));
        RadioButton rb3 = new RadioButton("Meuble");
        rb3.setToggleGroup(tg);
        rb3.setOnAction(new ControleurBoutonCate(this));
        RadioButton rb4 = new RadioButton("Outil");
        rb4.setToggleGroup(tg);
        rb4.setOnAction(new ControleurBoutonCate(this));
        VBox vbRadioButton = new VBox();
        vbRadioButton.getChildren().addAll(rb1,rb2,rb3,rb4);
        TitledPane tp = new TitledPane("Catégorie :",vbRadioButton);

        //mise en forme
        this.descriptionObjetAVendre.setMaxWidth(400);
        this.nomObjetAVendre.setMaxWidth(400);
        this.prixObjetAVendre.setMaxWidth(400);

        //FileChooser 
        this.fc = new FileChooser();
        this.fc.setTitle("Choix de l'image");
        FileChooser.ExtensionFilter et = new FileChooser.ExtensionFilter("Image","*.img","*.jpg","*.png");
        this.fc.getExtensionFilters().add(et);
        
        this.ImageObjetAVendre.setFitWidth(180);
        this.ImageObjetAVendre.setFitHeight(210);

        //Mise en page dans le GridPane
        GridPane gp = new GridPane();

        gp.add(l1,0,0);
        gp.add(this.nomObjetAVendre,0,1);
        gp.add(l2,0,2);
        gp.add(this.descriptionObjetAVendre,0,3);
        gp.add(l3,0,4);
        gp.add(this.prixObjetAVendre,0,5);
        gp.add(tp,0,6);
        gp.add(l5,0,7);
        gp.add(this.prixMinObjetAVendre,0,8);
        gp.add(bCreer,0,9);
        gp.add(l6,1,0);
        gp.add(this.tempsAvantMiseEnVente,1,1);
        gp.add(l7,1,2);
        gp.add(this.DureeDeVente,1,3);
        gp.add(l4,1,4);
        gp.add(bImage,1,5);
        gp.add(this.ImageObjetAVendre,1,6);
        gp.setVgap(10);
        gp.setHgap(30);

        VBox vb = new VBox();
        vb.getChildren().addAll(titre,gp);

        //Mise en forme
        VBox.setMargin(titre, new Insets(5,0,5,15));
        VBox.setMargin(gp,new Insets(15,0,10,10));
        gp.setBackground(new Background(new BackgroundFill(Color.rgb(230,230,230), null, null)));
        gp.setPadding(new Insets(15));

        return  vb;

    }

    /**
     * Métode qui permet de créer le Pane des articles en vente de l'utilisateur grâce à la méthode annonce
     * @return Le Pane de l'espace des articles en ventes 
     */
    public Pane espaceArticlesEnVentes() {
        //Création titre
        Text titre = new Text("Articles en ventes : ");
        titre.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        // VBox principale 
        VBox vb = new VBox();
        VBox.setMargin(titre,new Insets(10,0,20,15));

        //Ajout de toute les annonces à une VBox 
        VBox vb2 = new VBox();
        for (Annonce a : this.lesAnnonces) {
            Pane paneAnnonce = this.annonce(a.getNom(), a.getImg(), a.getDescription(), a.getPrix());
            vb2.getChildren().add(paneAnnonce);
            VBox.setMargin(paneAnnonce,new Insets(10,0,0,10));
        }

        //Mise en page dans un ScrollPane 
        ScrollPane sp = new ScrollPane(vb2);
        sp.setPrefWidth(350);
        sp.setPrefHeight(570);

        //ajout des éléments 
        vb.getChildren().addAll(titre,sp);

        return vb;

    }

    /**
     * Permet de créer la page de vente
     * @return Le Pane correspondant à la page de vente
     */
    public Pane pageAnnonce() {
        // Hbox prncipale 
        HBox hb = new HBox();
        //création des deux parties 
        Pane p1 = this.espaceArticlesEnVentes();
        Pane p2 = this.espaceVente();
        //ajout des éléments 
        hb.getChildren().addAll(p1,p2);
        //Mise en forme 
        HBox.setMargin(p1,new Insets(10,5,0,5));
        HBox.setMargin(p2,new Insets(10,10,0,0));


        return hb;
    }

    public void rempliAnnonces() {
        List<Produit> lstProd = this.encherirBD.objetenVente(this.user.getIdUt());
        System.out.println(lstProd.size()+" prod");
        for (Produit prod : lstProd) {
            this.lesAnnonces.add(new Annonce(prod.getNom(), prod.getDescription(), prod.getImages(),prod.getDernierPrix().intValue()));
        }
    }

    /**
     * Métode qui permet d'afficher le FileChooser et d'enregistrer l'image séléctionné 
     */
    public void choixImage() {
        File f = this.fc.showOpenDialog(this.scene.getWindow());
        if (f == null) {
            System.out.println("Aucun fichier selec");
        }
        else {
            this.ImageObjetAVendre.setImage(new Image(f.toURI().toString()));
            System.out.println(f.toURI().toString());
        }
    }

    /**
     * Permet d'obtenir le nom de l'objet à vendre 
     * @return String : le nom de l'objet à vendre 
     */
    public String getNomObjetAVendre() {
        return this.nomObjetAVendre.getText();
    }

    /**
     * Permet d'obtenir la description de l'objet à vendre 
     * @return String : La description de l'objet à vendre 
     */
    public String getDescriptionObjetAVendre() {
        return this.descriptionObjetAVendre.getText();
    }

    /**
     * Permet d'obtenir le prix de départ de l'objet à vendre 
     * @return int : Le prix de l'objet à vendre 
     */
    public int getPrixObjetAVendre() {
        try {
            return Integer.parseInt(this.prixObjetAVendre.getText());
        }
        catch (NumberFormatException e) {
            this.popUpMessageNombre().showAndWait();
        }
        return 0;
        
    }

    /**
     * Permet d'obtenir le prix minimum de l'objet à vendre 
     * @return int : le prix de l'objet à vendre 
     */
    public int getPrixMinObjetAVendre() {
        try {
            return Integer.parseInt(this.prixMinObjetAVendre.getText());
        }
        catch (NumberFormatException e) {
            this.popUpMessageNombre().showAndWait();
        }
        return 0;
        
    }

    /**
     * Permet d'obtenir le nombre d'heure avant la mise en vente de l'objet 
     * @return int : temps avant la mise en vente 
     */
    public int getTempsAvantMiseEnVente() {
        try {
            return Integer.parseInt(this.tempsAvantMiseEnVente.getText());
        }
        catch (NumberFormatException e ) {
            this.popUpMessageNombre().showAndWait();
        }
        return 0;
        
    }

    /**
     * Permet d'obtenir le nombre d'heure de la durée de la vente 
     * @return int : nombre d'heure de la vente 
     */
    public int getDureeVente() {
        return Integer.parseInt(this.DureeDeVente.getText());
    }

    /**
     * Permet d'obtenir la catégorie de l'objet à vendre
     * @return String : Catégorie de l'objet 
     */
    public String getCateObjetAVendre() {
        return this.cateObjetAVendre;
    }

    public int getIdUtilisateur() {
        return this.user.getIdUt();
    }

    /**
     * Permet d'obtenir l'image de l'objet à vendre
     * @return Image : Image de l'objet
     */
    public ImageView getImageObjetAVendre() {
        return this.ImageObjetAVendre;
    }

    /**
     * Permet de modifier la catégorie de l'objet à vendre 
     * @param cate String : catégorie de l'objet à vendre 
     */
    public void setCateObjetAVendre(String cate) {
        this.cateObjetAVendre = cate;
    }

    public BorderPane fenetreConnxeion() {
        // Création de la fenêtre de connexion
        BorderPane panelPrincipal = new BorderPane();
        // Création des 3 parties de la fenêtre
        VBox vBoxCentrale = new VBox();
        Rectangle r = new Rectangle(300, 1000);
        // Style de la partie de gauche
        r.setFill(Color.web("#00F5A2"));
        // Creation des textes
        Text connexion = new Text("Connexion");
        connexion.setFont(Font.font("Arial", FontWeight.BOLD, 50));
        Text entreeId = new Text("Entrez vos identifiants pour vous connecter");
        entreeId.setFont(Font.font("Arial", 14));
        // Remplissage partie centrale
        vBoxCentrale.getChildren().addAll(connexion, entreeId, this.tMail, this.tPassword, this.bConnexion);
        // Style de la partie centrale
        vBoxCentrale.setSpacing(20);
        vBoxCentrale.setPrefWidth(400);
        vBoxCentrale.setPrefHeight(800);
        vBoxCentrale.setPadding(new Insets(0,0, 250, 290));
        vBoxCentrale.setAlignment(Pos.CENTER);
        // Style de la partie de droite
        this.lSignUp.setPrefWidth(400);
        this.lSignUp.setPrefHeight(800);
        this.lSignUp.setPadding(new Insets(10, 116, 0, -90));
        this.lSignUp.setTextAlignment(TextAlignment.RIGHT);
        // Remplissage de la fenêtre de connexion
        panelPrincipal.setLeft(r);
        panelPrincipal.setCenter(vBoxCentrale);
        panelPrincipal.setRight(this.lSignUp);
        return panelPrincipal;
    }

    public TextField getTMail() {
        return this.tMail;
    }

    public PasswordField getTPassword() {
        return this.tPassword;
    }

    public TextField gettMail(){
        return this.tMail;
    }
    
    public PasswordField gettPassword(){
        return this.tPassword;
    }

    public void modeInscription() {
        panelPrincipale.setCenter(this.pageEntiere());
    }

    public Utilisateur setUser(Utilisateur user){
        return this.user = user;
    }

    public void modeConnexion() {
        Pane connexion = this.fenetreConnxeion();
        this.panelPrincipale.setCenter(connexion);
        this.panelPrincipale.setTop(null);
        this.user = null;
        this.tMail.setText("");
        this.tPassword.setText("");
    }

    public void modeVente() {
        this.lesAnnonces.clear();
        this.rempliAnnonces();
        System.out.println(this.lesAnnonces.size());
        Pane vente = this.pageAnnonce();
        this.panelPrincipale.setCenter(vente);
        Pane hautDePage = this.ajouteHautDePage();
        this.panelPrincipale.setTop(hautDePage);
        this.panelPrincipale.setBottom(null);
    }

    public void connexion() {
        try {
            this.user = this.encherirBD.seConnecter(this.tMail.getText(),this.tPassword.getText());
        }
        catch (SQLException e ) {
            System.out.println("Erreur");
        }
        catch (ExceptionMailNonPresent e) {
            System.out.println("L'email n'est pas présent");
        }
        catch (ExceptionMdpIncorect e) {
            System.out.println("Mot de passe incorrect");
        }
        catch (ExceptionCompteDesactive e) {
            System.out.println("Le compte est désactivé");
        }
        if (this.user != null) {
            this.modeVente();
        }
    }
    /** 
     * Méthode permettant de créer le bandeau du haut de la page avec les onglets de navigations vers les autres pages et le champ de recherche
     * */ 
    public GridPane ajouteHautDePage() {
        GridPane root = new GridPane();
        HBox hbox1 = new HBox();
        GridPane.setRowIndex(hbox1, 0);
        GridPane.setColumnIndex(hbox1, 0);
        rempliHbox(hbox1);
        root.getChildren().add(hbox1); // Ajoute hbox1 à la grille
    
        BorderPane borderPane1 = new BorderPane();
        rempliBorderPane(borderPane1);
        GridPane.setRowIndex(borderPane1, 1);
        GridPane.setColumnIndex(borderPane1, 0);
        root.getChildren().add(borderPane1); // Ajoute borderPane1 à la grille

        return root;
    }
    /**
     * Rempli la hbox de la bordure avec les différents boutons
     * @param hbox la hbox situé dans le borderpane à remplir
     */
    protected void rempliHbox(HBox hbox) {
        // Crée le lien hypertexte VAE permettant de revenir au menu principal
        Hyperlink vae = new Hyperlink("VAE");
        vae.setTextFill(Color.BLACK);
        vae.setStyle("-fx-font-size: 20px;");
        vae.setOnMouseEntered(e -> vae.setUnderline(false));
        vae.setPadding(new Insets(10,100,10,10));
        // Création du bouton menu
        Button menu = creeMenuButton();


        //Crée le button profil avec son image correspondante
        Image profil = new Image("file:img/profil.png");
        ImageView profilView = new ImageView(profil);
        profilView.setFitWidth(25); // Définit une largeur pour l'image
        profilView.setFitHeight(20); // Définit une hauteur pour l'image
        Button buttonprofil = new Button();
        buttonprofil.setGraphic(profilView);
        buttonprofil.setStyle("-fx-background-color: transparent;");
        buttonprofil.setPadding(new Insets(10,105,10,10));
        buttonprofil.setOnAction(new ControleurBoutonProfil(this));

        TextField recherche = new TextField();
        this.recherche = recherche;
        recherche.setPromptText("RECHERCHE");
        recherche.setPadding(new Insets(5,105,10,10));
        recherche.setOnKeyPressed(new ControleurRecherche(this, encherirBD));

        Image loupe = new Image("file:img/loupe.png");
        ImageView loupeView = new ImageView(loupe);
        loupeView.setFitWidth(25); // Définit une largeur pour l'image
        loupeView.setFitHeight(20); // Définit une hauteur pour l'image
        Button rechercheButton = new Button();
        rechercheButton.setGraphic(loupeView);
        rechercheButton.setStyle("-fx-background-color: transparent;");
        rechercheButton.setPadding(new Insets(10,105,10,10));
        rechercheButton.setOnAction(new ControleurBoutonRechercheProduit(this, encherirBD));        
        

        //Création de la combobox comportant les différents filtres
        ComboBox<String> filtre = new ComboBox<>();
        filtre.getItems().addAll("Filtre 1", "Filtre 2", "Filtre 3");
        filtre.setPromptText("Filtrer par");
        filtre.setStyle("-fx-background-color: transparent;");
        filtre.setPadding(new Insets(10,105,10,10));

        //ajout de la combobox crée précédemment dans une hbox
        HBox menuFiltreBox = new HBox();
        menuFiltreBox.getChildren().add(filtre);
        HBox.setMargin(filtre, new Insets(10,100,10,10));
        menuFiltreBox.setStyle("-fx-background-color: transparent;");


        hbox.getChildren().addAll(vae, recherche,rechercheButton, menuFiltreBox, buttonprofil ,menu);
        hbox.setSpacing(20);
        // permet de mettre pour chaque noeud de la hbox une marge de 10 en haut
        for (Node node : hbox.getChildren()) {
            HBox.setMargin(node, new Insets(10,0,0,0));
        }
        
        BackgroundFill backgroundFill = new BackgroundFill(Color.rgb(59, 161, 117), null, null);
        hbox.setBackground(new javafx.scene.layout.Background(backgroundFill));
    }
    /**
     * Rempli le borderpane avec les différents liens hypertextes et le titre
     * @param borderpane le borderpane à remplir
     */
    public void rempliBorderPane(BorderPane borderpane){
        Text espaceClient = new Text("Espace Client");
        espaceClient.setStyle("-fx-font-size: 20px;");
        espaceClient.setFill(Color.BLACK);
        borderpane.setTop(espaceClient);
        HBox differentePage = new HBox();
        Hyperlink produit = new Hyperlink("Produits");
        produit.setOnAction(new ControleurProduit(this));
        Hyperlink annonces = new Hyperlink("Annonce");
        annonces.setOnAction(new ControleurPageVente(this));
        Hyperlink performance = new Hyperlink("Performances");
        performance.setOnAction(new ControleurPerformance(this));
        performance.setUnderline(false);
        this.modifieStyleLien(performance);
        this.modifieStyleLien(annonces);
        this.modifieStyleLien(produit);
        //Permet de gérer du positionnement de la hbox contenant les liens hypertextes permettant la navigation
        HBox.setMargin(produit, new javafx.geometry.Insets(0, 0, 0, 300));
        HBox.setMargin(differentePage, new javafx.geometry.Insets(20, 30, 30, 0));
        differentePage.setSpacing(200);
        differentePage.getChildren().addAll(produit, annonces, performance);
        borderpane.setBottom(differentePage);
        BackgroundFill backgroundFill = new BackgroundFill(Color.rgb(161, 227, 175), null, null);
        borderpane.setBackground(new javafx.scene.layout.Background(backgroundFill));
        
    }
    public void pagePerformance(){
        this.panelPrincipale.setCenter(ajouteMilieuDePage());
        this.panelPrincipale.setBottom(ajouteBasDePage());
    }
    /**
     * Permet de crée le Bouton qui lorsque celui-ci est cliqué permet d'afficher le menu
     * @return le bouton permettant d'afficher le menu
     */
    public Button creeMenuButton(){
        MenuItem menuItem1 = new MenuItem("Option 1");
        if (this.user.getRole() == 1){
            menuItem1 = new MenuItem("Page admin");
            menuItem1.setOnAction( event -> this.affichePageAdmin());
        }
        Button menubouton = new Button("☰");
        MenuItem menuItem2 = new MenuItem("Option 2");
        MenuItem menuItem3 = new MenuItem("Option 3");
        ContextMenu contextMenu = new ContextMenu(menuItem1, menuItem2, menuItem3);
        // Création de la barre de menu
        menubouton.setStyle("-fx-background-color: transparent;");
        menubouton.setStyle("-fx-font-size: 30px;");
        menubouton.setStyle("-fx-background-color: #3BA175;"); 
        menubouton.setPadding(new Insets(10,40,10,10));
        menubouton.setOnAction(new ControleurBoutonRecherche(this));
        this.menubouton = menubouton;
        menubouton.setOnMouseClicked(event -> {
            contextMenu.show(menubouton, event.getScreenX(), event.getScreenY());
        });
        return menubouton;
    }
    public Button getboutonmenu(){
        return this.menubouton;
    }
    public GridPane getGridPane(){
        return this.gridPane;
    }
    public TextField getRecherche(){
        return this.recherche;
    }
    /**
     * Permet de modifier le style d'un lien hypertexte 
     * @param lien le lien hypertexte dont on veut modifier le style
     */
    private void modifieStyleLien(Hyperlink lien){
        lien.setStyle("-fx-font-size: 20px;");
        lien.setTextFill(Color.BLACK);
        lien.setOnMouseEntered(e -> lien.setUnderline(false));
        lien.setOnMouseExited(e -> lien.setUnderline(false));
    }

    /**
     * Permet de crée le bas de la page performance
     * @return la hbox contenant le bas de la page performance
     */
    private HBox ajouteBasDePage(){
        HBox bas = new HBox();
        bas.setPrefHeight(300);
        bas.setPrefWidth(1200);
        bas.setStyle("-fx-background-color: #FFFFFF;");
        VBox coutvente = new VBox();
        this.rempliCoutVente(coutvente);
        HBox graphique = new HBox();
        PieChart chart = new PieChart();
        chart.setTitle("Les catégorie des objets acheté");
        List<Double> d = this.encherirBD.catObjetAchetes(getIdUtilisateur());
        double v1 = d.get(0);
        double v2 = d.get(1);
        double v3 = d.get(2);
        double v4 = d.get(3);
        double vTot = d.get(4);
        double v11 = 0;
        double v22 = 0;
        double v33 = 0;
        double v44 = 0;
        if (v1 > 0){
            v11 = v1/vTot;
        }
        if (v2 > 0){
            v22 = v2/vTot;
        }
        if (v3> 0){
            v33 = v3/vTot;
        }
        if (v4> 0){
            v44 = v4/vTot;
        }
        chart.getData().setAll(
        new PieChart.Data("Vêtement",v11*100),
        new PieChart.Data("Ustensile cuisine",v22*100),
        new PieChart.Data("Meuble",v33*100),
        new PieChart.Data("Outil",v44*100));
        chart.setStyle("-fx-background-color: #FFFFFF;");
        chart.applyCss();
        graphique.getChildren().addAll(creeLineChart(),chart);
        bas.getChildren().addAll(coutvente,graphique);
        return bas;
    }
    /**
     * Permet de crée la première vbox du bas de la page performance
     * @return la vbox contenant le bas de la page performance avec son graphique
     */
    private void rempliCoutVente(VBox vbox){
        PieChart chartcout = new PieChart();
        chartcout.setTitle("Validation des ventes");
        chartcout.getData().setAll(
        new PieChart.Data("Ventes réussies",40),
        new PieChart.Data("Ventes échouéss",3));
        chartcout.setLegendSide(Side.RIGHT);
        vbox.getChildren().add(chartcout);
        chartcout.applyCss();
        chartcout.lookup(".chart-legend").setStyle("-fx-background-color: #FFFFFF;"); // Changer la couleur de fond de la légende
        VBox.setMargin(chartcout, new Insets(0, 30, 0, 20));
        vbox.setStyle("-fx-background-color: #c9d4cc;");
        vbox.setStyle("-fx-border-color: c9d4cc; -fx-border-width: 20px;-fx-background-color: #FFFFFF;");
        // changer la cuoleur de fond du graphique
        
        
    }
    /**
     * Permet de crée la deuxième vbox du bas de la page performance avec les informations sur les impressions en incluant les séparateurs
     * @param vbox la vbox que l'on veut remplir
     */
    private void rempliImpression(VBox vbox){
        Text impression = new Text("Nombre de vente de ces 30 derniers jours");
        Text nbimpressionclient = this.encherirBD.acquisition30Jours(getIdUtilisateur()); 
        
        Separator separator1 = new Separator();
        Text impressionclient = new Text("Nombre d'objets vendus en tout par catégories");
        Text nbObjetObtenuParCat = this.encherirBD.nbObjetVendusParCat(getIdUtilisateur());
        Separator separator2 = new Separator();

        vbox.getChildren().addAll(impression,nbimpressionclient,separator1,impressionclient,nbObjetObtenuParCat,separator2);
        vbox.setStyle("-fx-background-color: #FFFFFF;");
        vbox.setPrefWidth(320);
        for(Node node: vbox.getChildren()){
            VBox.setMargin(node, new Insets(20, 0, 0, 10));
        }
        vbox.setStyle("-fx-border-color: c9d4cc; -fx-border-width: 20px;");

    }
    /**
     * permet de créer le milieu de la page performance
     * @return le gridpane contenant le milieu de la page performance
     */
    public GridPane ajouteMilieuDePage(){
        GridPane milieu = new GridPane();
        milieu.setPrefWidth(1200);
        milieu.setStyle("-fx-background-color: #c9d4cc;");
        Label performancedeventes = new Label("Performances et Acquisitions");
        performancedeventes.setStyle("-fx-background-color: #c9d4cc;-fx-font-size: 20px;");
        performancedeventes.setPrefWidth(1200);
        performancedeventes.setPadding(new Insets(10,10,0,20));
        milieu.add(performancedeventes, 0, 0);
        VBox niveauvendeur = new VBox();
        milieu.add(niveauvendeur, 0, 1);
        this.remplipremierVbox(niveauvendeur);
        VBox ventes = new VBox();
        milieu.add(ventes, 1, 1);
        this.remplideuxiemeVbox(ventes);
        niveauvendeur.setPrefWidth(200);
        niveauvendeur.setPadding(new Insets(20,20,0,20));
        VBox.setMargin(niveauvendeur, new Insets(20, 0, 0, 0));
        return milieu;
    }
    /**
     * Permet de remplir la deuxième vbox du milieu de la page performance en créant les graphiques
     * @param vbox la vbox que l'on veut remplir
     */
    private void remplideuxiemeVbox(VBox vbox){
        Label vente = new Label("Inventaire");
        vente.setStyle("-fx-font-size: 20px;");
        vente.setPadding(new Insets(20));
        vente.setStyle("-fx-background-color: #FFFFFF;");
        vente.setMinWidth(500);
        Separator separator1 = new Separator();
        
        VBox impression = new VBox();
        this.rempliImpression(impression);
        impression.setStyle("-fx-background-color: white;");
        vbox.getChildren().addAll(vente,separator1,impression);
        vbox.setStyle("-fx-background-color: #FFFFFF;");
        vbox.setStyle("-fx-border-color: #c9d4cc; -fx-border-width: 20px;");

    }
    /**
     * Permet de créer le LineChart contenu dans la deuxième vbox du milieu de la page performance
     */
    private LineChart<Number, Number> creeLineChart(){
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<Number, Number> linechart = new LineChart<>(xAxis, yAxis);
        linechart.setTitle("Dernières acquisitions");
        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        series.setName("Nombre d'acquisition'");
        List<Integer> g = this.encherirBD.dernieresAcquisition(getIdUtilisateur());
        series.getData().add(new XYChart.Data<>(1, g.get(0)));
        series.getData().add(new XYChart.Data<>(2, g.get(1)));
        series.getData().add(new XYChart.Data<>(3, g.get(2)));
        series.getData().add(new XYChart.Data<>(4, g.get(3)));
        series.getData().add(new XYChart.Data<>(5, g.get(4)));
        linechart.getData().add(series);
        linechart.setLegendVisible(false);
        linechart.setStyle("-fx-background-color: #FFFFFF;");
        return linechart;
    }
    private void remplipremierVbox(VBox vBox){
        //crée les différentes séparateur et leur insère un padding
        Separator separator1 = new Separator();
        Separator separator2 = new Separator();
        Separator separator3 = new Separator();
        Separator separator4 = new Separator();
        separator1.setPadding(new Insets(10,10,30,10));
        separator2.setPadding(new Insets(10,10,30,10));
        separator3.setPadding(new Insets(10,10,30,10));
        separator4.setPadding(new Insets(10,10,30,10));
        Text niveaudevendeur = new Text("Niveau de vendeur");
        niveaudevendeur.setStyle("-fx-font-size: 20px;");
        String performance = "Performance";
        // if(Encherir.objetenvente().isEmpty()){
            // String performance = "Mauvaise Performance";
        // }
        // else{
            // String performance = "Bonne Performance";
        // }s
        Text perf = new Text(performance);
        String expedie = "Expedie";
        String nb = "9.93"; // A remplacer avec l'utilisation de la base de données
        expedie =nb + "% "+expedie;
        Text exp = new Text(expedie);
        String transaction = "Montant moyen des enchères : ";
        try{
            PreparedStatement ps = connexionMySQL.prepareStatement("select avg(montant) from ENCHERIR where idut = ?;");
            ps.setInt(1,getIdUtilisateur());
            ResultSet res = ps.executeQuery();
            res.next();
            transaction = transaction + res.getInt(1)+" €";

        }
        catch(Exception e){}
        Text moyenMontantTransac = new Text(transaction);
        String montanttransaction = "Montant totale des transactions";
        Text montanttransac = new Text(montanttransaction);
        vBox.getChildren().addAll(niveaudevendeur,separator1, perf,separator2, exp,separator3 ,moyenMontantTransac,separator4, montanttransac);
        vBox.setStyle("-fx-background-color: white; -fx-border-color: c9d4cc; -fx-border-width: 20px;");
            vBox.setPrefWidth(200);
    }
    private HBox pageGauche(){
        HBox pane = new HBox();
        Rectangle gauche = new Rectangle(300, 1000);
        gauche.setFill(Color.web("#00F5A2"));
        pane.getChildren().add(gauche);
        return pane;
    }

    private BorderPane pageDroite(){
        BorderPane pane = new BorderPane();
        VBox info = Info();
        BorderPane.setMargin(info, new Insets(300, 0, 0, 250));
        pane.setCenter(info);
        this.contenu = membre();
        pane.setRight(this.contenu);
        return pane;
    }

    private HBox membre(){
        HBox pane = new HBox();
        Label mb = new Label("Déjà membre ?");
        mb.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        Label conn = new Label("Connectez-vous");
        conn.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        Button connexion = new Button();
        connexion.setGraphic(conn);
        connexion.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: #00F5A2;");
        connexion.setOnAction(new ControleurRetourConnexion(this));
        HBox.setMargin(mb, new Insets(17, 30, 0, -100));
        HBox.setMargin(connexion, new Insets(10, 20, 0, -10));
        pane.getChildren().addAll(mb,connexion);
        return pane;
    }

    private VBox Info(){
        VBox pane = new VBox();
        pane.setMinWidth(350);
        Label connexion = new Label("Page d'inscription");
        Label cree = new Label("Veuillez remplir les champs en dessous:");
        this.pseudo.setPromptText("Choisissez un pseudonyme visible par tous"); 
        this.pseudo.setMinHeight(35);
        this.email.setPromptText("Saisissez votre email"); 
        this.email.setMinHeight(35);
        this.mdp1.setPromptText("Saisissez votre mot de passe");
        this.mdp1.setMinHeight(35);
        this.mdp2.setPromptText("Confirmer votre mot de passe"); 
        this.mdp2.setMinHeight(35);
        Label inscript = new Label("S'inscrire");
        inscript.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        this.creation.setGraphic(inscript);
        this.creation.setMinWidth(175);
        this.creation.setMinHeight(40);
        creation.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: #00F5A2;");
        connexion.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 41px;");
        cree.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        VBox.setMargin(connexion, new Insets(-100,0, 20, -5));
        VBox.setMargin(cree, new Insets(0,0, 35, 40));
        VBox.setMargin(this.pseudo, new Insets(0,0, 15, -40));
        VBox.setMargin(this.email, new Insets(0,0, 15, -40));
        VBox.setMargin(this.mdp1, new Insets(0,0, 15, -40));
        VBox.setMargin(this.mdp2, new Insets(0,0, 35, -40));
        VBox.setMargin(creation, new Insets(0,0, 0, 125));
        pane.getChildren().addAll(connexion,cree,this.pseudo,this.email,this.mdp1,this.mdp2,this.creation);
        return pane;
    }

    public Alert popUpMessageChampsVide(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Champs Vide");  
        return alert;
    }

    public Alert popUpMessageNombre(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Un chiffre n'est pas correct");  
        return alert;
    }

    public Alert popUpMessageMdpDiff(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Mot de passe différent");  
        return alert;
    }

    public Alert popUpMessageEmail(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Email incorrect");  
        return alert;
    }

    public Alert popUpMessageMdpPasComplex(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Mot de passe pas assez complex, il faut au moins une majuscule, une minuscule et un chiffre");  
        return alert;
    }

    public Alert popUpMessageErreur(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Erreur popUpMessageErreur");  
        return alert;
    }

    public static boolean isValidEmail(String email) {
        if (email.length() < 30){
            return EMAIL_PATTERN.matcher(email).matches();
        }
        return false;
    }

    // public static String cryptage(String password) {
    //     StringBuilder encryptedPassword = new StringBuilder();
    //     for (int i = 0; i < password.length(); i++) {
    //         char currentChar = password.charAt(i);
    //         char encryptedChar = (char) (currentChar + 7);
    //         encryptedPassword.append(encryptedChar);
    //     }
    //     return encryptedPassword.toString();
    // }

    // public static String decryptage(String encryptedPassword) {
    //     StringBuilder decryptedPassword = new StringBuilder();
    //     for (int i = 0; i < encryptedPassword.length(); i++) {
    //         char currentChar = encryptedPassword.charAt(i);
    //         char decryptedChar = (char) (currentChar - 7);
    //         decryptedPassword.append(decryptedChar);
    //     }
    //     return decryptedPassword.toString();
    // }
    
    public static boolean isPasswordComplex(String password) {
        if (password.length() < 5 || password.length() > 30) {
            return false;
        }
        
        boolean maj = false;
        boolean minus = false;
        boolean nb = false;
        
        for (char c : password.toCharArray()) {
            if (Character.isUpperCase(c)) {
                maj = true;
            } else if (Character.isLowerCase(c)) {
                minus = true;
            } else if (Character.isDigit(c)) {
                nb = true;
            }
        }
        if (maj == true && minus == true && nb == true){
            return true;
        }
        else{
            return false;
        }
    }
    public TextField getPseudo() {
        return pseudo;
    }

    public TextField getEmail() {
        return email;
    }

    public PasswordField getMdp1() {
        return mdp1;
    }

    public PasswordField getMdp2() {
        return mdp2;
    }
    private BorderPane pageEntiere(){
        BorderPane pane = new BorderPane();
        this.pagededroite = pageDroite();
        this.pagedegauche = pageGauche();
        pane.setCenter(this.pagededroite);
        pane.setLeft(this.pagedegauche);
        return pane;
    }
    public void affichePageOffre(){   
        ScrollPane page = new ScrollPane();
        VBox contenu = new VBox();
        page.setContent(contenu);
        for (Produit pdt : this.produits){
            contenu.getChildren().add(afficheFicheProduit(pdt));
        }
        this.panelPrincipale.setCenter(page);
        this.panelPrincipale.setBottom(null);
    }

    public void remplieProd() {
        for (Produit prod : this.encherirBD.getListeProd()) {
            this.produits.add(prod);
        }
    }

    public HBox afficheFicheProduit(Produit pdt){
        HBox HBoxPrincpale = new HBox();
        VBox VBoxPrix = new VBox();
        VBox VBoxInfosVendeur = new VBox();
        VBox VBoxEnchere = new VBox();
        VBox sansImage = new VBox();
        HBox InfosBas = new HBox();
        // Création de l'image
        Image image = pdt.getImages();
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(300);
        imageView.setFitWidth(300);
        // Création texte description
        Text description = new Text(pdt.getDescription());
        // Style texte description
        description.setWrappingWidth(900);
        description.setFont(Font.font("Arial", FontWeight.NORMAL, 15));
        // Création zone prix
        Text nomProduit = new Text(pdt.getNom());
        Text prixActuel = new Text("Prix actuel : ");
        Text dernierPrix = new Text(pdt.getDernierPrix()+"€");
        // Style zone prix
        nomProduit.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        prixActuel.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        dernierPrix.setFont(Font.font("Arial", FontWeight.BOLD, 40));
        // Création zone infos vendeur
        Text nomVendeur = new Text("De : " + pdt.getUser().getPseudoUt());
        Text nbAnnonces = new Text("annonces : " + pdt.getUser().getNbAnnonces());
        Text nbVentes = new Text("ventes : " + pdt.getUser().getNbVente());
        // Style zone infos vendeur
        nomVendeur.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        nbAnnonces.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        nbVentes.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        // Création zone enchere
        Button boutonEnchere = new Button("Faire une offre");
        TextField prixEnchere = new TextField();
        prixEnchere.setPromptText("Prix");
        // Style zone enchere
        boutonEnchere.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        boutonEnchere.setStyle("-fx-background-radius: 30; ");
        boutonEnchere.setTextFill(Color.WHITE);
        boutonEnchere.setStyle("-fx-background-color: #3164f4;");
        prixEnchere.setStyle("-fx-font-size: 20px;");
        prixEnchere.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        // Taille Boxes
        sansImage.setPrefSize(900, 300);
        VBoxPrix.setPrefSize(300, 200);
        VBoxInfosVendeur.setPrefSize(300, 200);
        VBoxEnchere.setPrefSize(300, 200);
        VBoxPrix.setPadding(new Insets(10));
        VBoxInfosVendeur.setPadding(new Insets(10));
        VBoxEnchere.setPadding(new Insets(10));
        VBoxPrix.setSpacing(10);
        VBoxInfosVendeur.setSpacing(10);
        VBoxEnchere.setSpacing(10);
        // Remplissage de la grille
        VBoxPrix.getChildren().addAll(nomProduit ,prixActuel, dernierPrix);
        VBoxInfosVendeur.getChildren().addAll(nomVendeur, nbAnnonces, nbVentes);
        VBoxEnchere.getChildren().addAll(boutonEnchere, prixEnchere);
        InfosBas.getChildren().addAll(VBoxPrix, VBoxInfosVendeur, VBoxEnchere);
        sansImage.getChildren().addAll(description, InfosBas);
        HBoxPrincpale.getChildren().addAll(imageView, sansImage);
        HBox.setMargin(imageView, new Insets(20,0,0,0));
        HBox.setMargin(sansImage, new Insets(20,0,0,0));
        // Ajout des boutons dans les HashMap
        boutons.put(pdt, boutonEnchere);
        champs.put(pdt, prixEnchere);
        // Ajout des controlleurs
        boutonEnchere.setOnAction(new ControlleurBoutonEnchere(this, pdt));
        return HBoxPrincpale;
    }

    public void miseAJourVisuel(){
        ScrollPane scrollpane = (ScrollPane) this.panelPrincipale.getCenter();
        VBox contenu = (VBox) scrollpane.getContent();
        contenu.getChildren().clear();
        for (Produit pdt : this.produits){
            contenu.getChildren().add(afficheFicheProduit(pdt));
        }
    }

    public void affichePageAdmin(){
        ScrollPane page = new ScrollPane();
        VBox contenu = new VBox();
        page.setContent(contenu);
        for (Utilisateur ut : this.encherirBD.gUtilisateurs()){
            HBox ligne = new HBox();
            Button boutonsupprimer = new Button("🗶");
            boutonsupprimer.setOnAction(event ->
            {
                this.encherirBD.supprimerUtilisateur(ut);
                this.affichePageAdmin();
            });
            Text txt1 = new Text(ut.getPseudoUt());
            Text txt2 = new Text(ut.getEmailUt());
            Text txt3 = new Text(ut.getMdpUt());
            VBox v1a = new VBox();
            v1a.setMinWidth(100);
            v1a.getChildren().addAll(txt1);
            VBox v2a = new VBox();
            v2a.setMinWidth(200);
            v2a.getChildren().addAll(txt2);
            VBox v3a = new VBox();
            v3a.getChildren().addAll(txt3);
            v3a.setMinWidth(300);
            ligne.getChildren().addAll(v1a, v2a, v3a,boutonsupprimer);
            contenu.getChildren().addAll(ligne, new Separator());
            
        }
        panelPrincipale.setCenter(page);
        panelPrincipale.setBottom(null);
    }

    public String getValeurEnchere(Produit pdt){
        for (Produit produit : champs.keySet()) {
            if (produit.equals(pdt)){
                return champs.get(produit).getText();
            }
        }
        return "Erreur";
    }

    /**
     * Permet d'obtnir le lien avec la base de donnée 
     * @return EncherirBD
     */
    public EncherirBD getEncherirBD() {
        return this.encherirBD;
    }

    public void modeProfilUtilisateur() {
        panelPrincipale.setCenter(this.pageProfilUtilisateur());
        panelPrincipale.setBottom(null);
    }

    public Utilisateur getUser(){
        return this.user;
    }


    private BorderPane pageProfilUtilisateur(){
        BorderPane pane = new BorderPane();
        this.pagededroite = pageDroiteProfilUtilisateur();
        this.pagedegauche = pageGaucheProfilUtilisateur();
        pane.setCenter(this.pagededroite);
        pane.setLeft(this.pagedegauche);
        return pane;
    }

    private HBox pageGaucheProfilUtilisateur(){
        HBox pane = new HBox();
        Rectangle gauche = new Rectangle(300, 1000);
        gauche.setFill(Color.web("#00F5A2"));
        pane.getChildren().addAll(gauche);
        return pane;
    }

    private BorderPane pageDroiteProfilUtilisateur(){
        BorderPane pane = new BorderPane();
        VBox nompage = pagetitre();
        HBox info = AffichageProfilUtilisateur();
        BorderPane.setMargin(info, new Insets(300, 0, 0, 250));
        pane.setTop(nompage);
        pane.setCenter(info);
        return pane;
    }

    private VBox pagetitre(){
        VBox pane = new VBox();
        Label nompage1 = new Label("Page d'information et de modification");
        Label nompage2 = new Label("de votre compte");
        nompage1.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 30px;");
        nompage2.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 30px;");
        VBox.setMargin(nompage1, new Insets(0, 0, 0, 145));
        VBox.setMargin(nompage2, new Insets(0, 0, 0, 330));
        pane.getChildren().addAll(nompage1,nompage2);
        return pane;
    }


    private HBox AffichageProfilUtilisateur(){
        HBox pane = new HBox();
        VBox Categorie = CategorieProfilUtilisateur();
        VBox Information = InfoProfilUtilisateur();
        VBox BoutonPourReset = BoutonPourResetProfilUtilisateur();
        pane.getChildren().addAll(Categorie,Information,BoutonPourReset);
        return pane;
    }

    private VBox CategorieProfilUtilisateur(){
        VBox pane = new VBox();
        pane.setMinWidth(350);
        Label pseudo = new Label("Pseudonyme:");
        Label email = new Label("Email:");
        Label mdp = new Label("Mot de passe:");
        Label statut = new Label("Statut:");
        Label VoirMDPLabel = new Label("Voir le MDP");
        VoirMDPLabel.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        Button VoirMDP = new Button();
        VoirMDP.setGraphic(VoirMDPLabel);
        VoirMDP.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: #3BA175;");
        VoirMDP.setOnAction(new ControleurAfficherMDP(this));
        pseudo.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 21px; -fx-text-fill: blue;");
        email.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 21px; -fx-text-fill: blue;");
        mdp.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 21px; -fx-text-fill: blue;");
        statut.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 21px; -fx-text-fill: blue;");
        VBox.setMargin(pseudo, new Insets(-150, 0, 50, -200));
        VBox.setMargin(email, new Insets(0, 0, 50, -200));
        VBox.setMargin(mdp, new Insets(0, 0, 50, -200));
        VBox.setMargin(statut, new Insets(0, 0, 50, 100));
        VBox.setMargin(VoirMDP, new Insets(0, 0, 15, -200));
        pane.getChildren().addAll(pseudo,email,mdp,statut,VoirMDP);
        return pane;
    }

    public Alert popUpMessageVoirMDP(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Voici votre mot de passe: " + this.user.getMdpUt());  
        return alert;
    } 

    private VBox InfoProfilUtilisateur(){
        VBox pane = new VBox();
        pane.setMinWidth(350);
        String motdepasse = this.user.getMdpUt();
        String motdepassecrypte = "";
        for (int i = 0; i < motdepasse.length(); i++){
            motdepassecrypte+="*";
        }
        Label pseudo = new Label(this.user.getPseudoUt());
        Label email = new Label(this.user.getEmailUt());
        Label mdp = new Label(motdepassecrypte);
        String stat = "Inactif";
        if (this.user.isActive()){
            stat = "Actif";
        }
        Label statut = new Label(stat);
        pseudo.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 21px;");
        email.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 21px;");
        mdp.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 21px;");
        statut.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 21px;");
        VBox.setMargin(pseudo, new Insets(-150, 0, 50, -360));
        VBox.setMargin(email, new Insets(0, 0, 50, -455));
        VBox.setMargin(mdp, new Insets(0, 0, 50, -360));
        VBox.setMargin(statut, new Insets(0, 0, 50, -120));
        pane.getChildren().addAll(pseudo,email,mdp,statut);
        return pane;
    }

    private VBox BoutonPourResetProfilUtilisateur(){
        VBox pane = new VBox();
        Label ChangerPseudoLabel = new Label("Changer le pseudonyme");
        ChangerPseudoLabel.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        Label ChangerEmailLabel = new Label("Changer l'email");
        ChangerEmailLabel.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        Label ChangerMDPLabel = new Label("Changer le mot de passe");
        ChangerMDPLabel.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 14px;");
        Button ChangerPseudo = new Button();
        Button ChangerEmail = new Button();
        Button ChangerMDP = new Button();
        ChangerPseudo.setGraphic(ChangerPseudoLabel);
        ChangerEmail.setGraphic(ChangerEmailLabel);
        ChangerMDP.setGraphic(ChangerMDPLabel);
        ChangerPseudo.setMinWidth(220);
        ChangerEmail.setMinWidth(220);
        ChangerMDP.setMinWidth(220);
        ChangerPseudo.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: #3BA175;");
        ChangerEmail.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: #3BA175;");
        ChangerMDP.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: #3BA175;");
        ChangerPseudo.setOnAction(new ControleurChangementUtilisateur(this,"Pseudo"));
        ChangerEmail.setOnAction(new ControleurChangementUtilisateur(this,"Email"));
        ChangerMDP.setOnAction(new ControleurChangementUtilisateur(this,"MDP"));
        VBox.setMargin(ChangerPseudo, new Insets(-150, 0, 50, -320));
        VBox.setMargin(ChangerEmail, new Insets(0, 0, 50, -320));
        VBox.setMargin(ChangerMDP, new Insets(0, 0, 50, -320));
        Label dc = new Label("Déconnexion du compte");
        dc.setStyle("-fx-font-family: Arial; -fx-font-weight: bold; -fx-font-size: 18px;");
        Button deco = new Button();
        deco.setGraphic(dc);
        deco.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: #F08080;");
        deco.setOnAction(new ControleurDeconnexion(this));
        VBox.setMargin(deco, new Insets(140, 0, 0, -635));
        pane.getChildren().addAll(ChangerPseudo,ChangerEmail,ChangerMDP,deco);
        return pane;
    }

    public void changePseudo(){
        try{
            if(!this.encherirBD.ListedesPseudos().contains(this.nouveauPseudo.getText())){
                try {
                    int idprofil = this.encherirBD.getIdprofil(this.user.getEmailUt(),this.user.getPseudoUt());
                    this.encherirBD.changerLePseudo(this.nouveauPseudo.getText(),idprofil);
                    this.user.setPseudo(this.nouveauPseudo.getText());
                    modeProfilUtilisateur();
                }
                catch (SQLException e ) {
                    System.out.println("Erreur changePseudo");
                }
            }
            else {
                popUpMessagePseudoUtilise().showAndWait();;
            }
        }
        catch (SQLException e ) {
            System.out.println("Erreur contains");
        }
    }

    public Alert popUpMessagePseudoUtilise(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Pseudo déjà utilisé");  
        return alert;
    }  

    public Alert popUpNewPseudo() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Veuillez saisir votre nouveau pseudo:");
        TextField champ = new TextField();
        champ.setPromptText("Choisissez un nouveau pseudonyme");
        alert.getDialogPane().setContent(champ);
        if (champ.equals(null)){
            return alert;
        }
        this.nouveauPseudo = champ;
        return alert;
    }

    public void changeEmail(){
        try {
            if (isValidEmail(this.nouvelEmail.getText()) && !this.encherirBD.ListedesEmails().contains(this.nouvelEmail.getText())){
                try {
                    int idprofil = this.encherirBD.getIdprofil(this.user.getEmailUt(),this.user.getPseudoUt());
                    this.encherirBD.changerLEmail(this.nouvelEmail.getText(),idprofil);
                    this.user.setEmail(this.nouvelEmail.getText());
                    modeProfilUtilisateur();
                }
                catch (SQLException e ) {
                    System.out.println("Erreur changeEmail");
                }
            }
            else {
                popUpMessageEmailChangement().showAndWait();;
            }
        }
        catch (SQLException e ) {
            System.out.println("Erreur contains");
        }
    }

    public Alert popUpMessageEmailChangement(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Email incorrect ou déjà relier à un compte");  
        return alert;
    }  

    public Alert popUpNewEmail() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Veuillez saisir votre nouvel Email:");
        TextField champ = new TextField();
        champ.setPromptText("Choisissez un nouvel Email");
        alert.getDialogPane().setContent(champ);
        if (champ.equals(null)){
            return alert;
        }
        this.nouvelEmail = champ;
        return alert;
    }

    public void changeMDP(){
        if (isPasswordComplex(this.nouveauMDP.getText())){
            try {
                int idprofil = this.encherirBD.getIdprofil(this.user.getEmailUt(),this.user.getPseudoUt());
                this.encherirBD.changerLeMDP(this.nouveauMDP.getText(),idprofil);
                this.user.setMdp(this.nouveauMDP.getText());
                modeProfilUtilisateur();
            }
            catch (SQLException e ) {
                System.out.println("Erreur changeMDP");
            }
        }
        else {
            popUpMessageMdpPasComplex().showAndWait();
        }
    }

    public Alert popUpNewMDP() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Veuillez saisir votre nouveau mot de passe:");
        PasswordField champ = new PasswordField();
        champ.setPromptText("Choisissez un nouveau mot de passe");
        alert.getDialogPane().setContent(champ);
        if (champ.equals(null)){
            return alert;
        }
        this.nouveauMDP = champ;
        return alert;
    }

    public Alert popUpMessageMdpIncorrect(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);      
        alert.setHeaderText("Mot de passe incorrect");  
        return alert;
    }

    public void setProduits(ArrayList<Produit> produits){
        this.produits = produits;
    }



    public static void main(String[] args) {
        launch(args);
    }
    
}
