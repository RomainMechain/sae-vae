import java.nio.ByteBuffer;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.serial.SerialBlob;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritablePixelFormat;
import java.io.ByteArrayInputStream;
import java.io.*;
import java.sql.Blob;
import java.sql.SQLException;
import javafx.scene.text.*;
import java.util.Collections;

public class EncherirBD {
    private ConnexionMySQL connexion;

    public EncherirBD(ConnexionMySQL connexion) {
        this.connexion = connexion;
    }

    /**
     * Méthode qui permet de récupérer un utilisateur a partir des éléments de la page de connexion 
     * @param mail String : le mail 
     * @param mdp String : le mot de passe 
     * @return
     * @throws SQLException
     * @throws ExceptionMailNonPresent Indique que le mail n'est pas présent dans la base de donnée
     * @throws ExceptionMdpIncorect Indique que le mail est présent mais le mot de passe ne correspond pas 
     */
    public Utilisateur seConnecter(String mail, String mdp) throws SQLException, ExceptionMailNonPresent, ExceptionMdpIncorect, ExceptionCompteDesactive {
        Statement s = this.connexion.createStatement();
        ResultSet r = s.executeQuery("Select emailUt from UTILISATEUR;");
        List<String> lstEmail = new ArrayList<>();
        while(r.next()) {
            lstEmail.add(r.getString(1));
        }
        if (lstEmail.contains(mail)) {
            PreparedStatement ps = this.connexion.prepareStatement("Select * from UTILISATEUR where emailUt = ?");
            ps.setString(1,mail);
            ResultSet r2 = ps.executeQuery();
            r2.next();
            if (! r2.getString("mdpUt").equals(mdp)) {
                throw new ExceptionMdpIncorect();
            }
            else {
                if (r2.getString("activeUt").charAt(0) == 'O') {
                    Utilisateur use =  new Utilisateur(r2.getInt("idUt"), r2.getString("pseudoUt"), r2.getString("emailUt"), r2.getString("mdpUt"), true);
                    if (r2.getInt("idrole") == 1) {
                        use.setRole(1);
                    }
                    return use;
                }
                else {
                    throw (new ExceptionCompteDesactive());
                }
            }
        }
        else {
            throw new ExceptionMailNonPresent();
        }
        
    }

    /**
     * Méthode qui retourne l'id Utilisateur maximum 
     * @return int : plus grand id utilisateur 
     * @throws SQLException
     */
    public int idUtMax() throws SQLException {
        Statement s = this.connexion.createStatement();
        ResultSet r = s.executeQuery("SELECT max(idUt) from UTILISATEUR");
        r.next();
        return r.getInt(1);
    }

    /**
     * Permet de rajouter un utilisateur dans la base de donnée grâce aux informations de la page d'inscription 
     * @param idUt int : id de l'utilisateur 
     * @param pseudoUt String : pseudo de l'utilisateur 
     * @param emailUt String : mail de l'utilisateur 
     * @param mdput String : mot de passe de l'utilisateur 
     * @throws SQLException
     */
    public void ajouteUtilisateur(int idUt, String pseudoUt, String emailUt,String mdput) throws SQLException {
        PreparedStatement ps = this.connexion.prepareStatement("INSERT INTO UTILISATEUR VALUES (?,?,?,?,'O',2);");
        ps.setInt(1,idUt);
        ps.setString(2, pseudoUt);
        ps.setString(3, emailUt);
        ps.setString(4, mdput);
        ps.executeUpdate();
    }
    
    public ArrayList<Produit> objetenVente(int idUt){
        ArrayList<Produit> liste = new ArrayList<Produit>();

        try {
            PreparedStatement statement = connexion.prepareStatement("select idOb,idUt,nomOb,descriptionOb,nomcat,imgph from OBJET natural join VENTE natural join CATEGORIE natural left join PHOTO where idUt = ?;");
        statement.setInt(1, idUt);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            int idOb = resultSet.getInt("idOb");
            int prix = this.getmaxmontant(idOb);
            if (prix == 0) {
                Statement s = this.connexion.createStatement();
                ResultSet r2 = s.executeQuery("select prixMin FROM VENTE where idOb = "+idOb+";");
                r2.next();
                prix = r2.getInt(1);
            }
            System.out.println(prix);
            liste.add(new Produit(idOb,resultSet.getString("nomOb"),resultSet.getString("descriptionOb"),EncherirBD.convertBlobToImage(resultSet.getBlob("imgph")),this.gUtilisateur(resultSet.getInt("idUt")),prix,new ArrayList<>(),resultSet.getString("nomcat")));
        }   
        } catch (Exception e) {
            // TODO: handle exception
        }
        return liste;
    }

    public List<Produit> getListeProd() {
        List<Produit> lst = new ArrayList<>();

        try {
            PreparedStatement statement = this.connexion.prepareStatement("select idOb,idUt,nomOb,descriptionOb,nomCat,imgph from OBJET natural join VENTE natural join CATEGORIE natural left join PHOTO LIMIT 10;");
            ResultSet r = statement.executeQuery();
            while(r.next()) {
                int idOb = r.getInt("idOb");
                int prix = this.getmaxmontant(idOb);
                if (prix == 0) {
                    Statement s = this.connexion.createStatement();
                    ResultSet r2 = s.executeQuery("select prixMin FROM VENTE where idOb = "+idOb+";");
                    r2.next();
                    prix = r2.getInt(1);
                }
                lst.add(new Produit(idOb,r.getString("nomOb"),r.getString("descriptionOb"),new Image("Loupe.png"),this.gUtilisateur(r.getInt("idUt")),prix,new ArrayList<>(),r.getString("nomcat")));
            }
        }
        catch (Exception e) {
            System.out.println("erreur");
        }
        return lst;
    }

    public static Image convertBlobToImage(Blob blob) throws SQLException, IOException {
        // Obtenez la taille du blob
        int blobLength = (int) blob.length();

        // Obtenez le tableau de bytes du blob
        byte[] blobAsBytes = blob.getBytes(1, blobLength);

        // Créez un flux d'entrée à partir du tableau de bytes
        try (InputStream is = new ByteArrayInputStream(blobAsBytes)) {
            // Créez un objet Image à partir du flux d'entrée
            return new Image(is);
        }
    }



    public Utilisateur gUtilisateur(int idUt){
        try {
        PreparedStatement statement = connexion.prepareStatement("select * from UTILISATEUR where idUt = ?;");
        statement.setInt(1, idUt);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        
            return new Utilisateur(resultSet.getInt("idUt"),resultSet.getString("pseudoUt"),resultSet.getString("emailUt"),resultSet.getString("mdpUt"),this.estActif(resultSet.getString("activeUt")));
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    public boolean estActif(String activeUt){
        return activeUt.equals("O");
    }
    public ArrayList<Utilisateur> gUtilisateurs(){
        ArrayList<Utilisateur> liste = new ArrayList<Utilisateur>();
        try {
            PreparedStatement statement = connexion.prepareStatement("select * from UTILISATEUR;");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                liste.add(new Utilisateur(resultSet.getInt("idUt"),resultSet.getString("pseudoUt"),resultSet.getString("emailUt"),resultSet.getString("mdpUt"),this.estActif(resultSet.getString("activeUt"))));
            }
        } catch (Exception e) {

        }
        return liste;
    }
    public void supprimerUtilisateur(Utilisateur ut){
        try {
            PreparedStatement statement = connexion.prepareStatement("delete from UTILISATEUR where idUt = ?;");
            statement.setInt(1, ut.getIdUt());
            statement.executeUpdate();
        } catch (Exception e) {
        }
    }
      /**
     * Méthode qui retourne l'id de vente maximum 
     * @return int : plus grand id de vente
     * @throws SQLException
     */
    public int idVeMax() throws SQLException {
        Statement s = this.connexion.createStatement();
        ResultSet r = s.executeQuery("SELECT max(idVe) from VENTE");
        r.next();
        return r.getInt(1);
    }

    /**
     * Méthode qui retourne l'id d'objet maximum 
     * @return int : plus grand id Objet 
     * @throws SQLException
     */
    public int idObMax() throws SQLException {
        Statement s = this.connexion.createStatement();
        ResultSet r = s.executeQuery("SELECT max(idOb) from OBJET");
        r.next();
        return r.getInt(1);
    }

    /**
     * Méthode qui retourne l'id photo maximum 
     * @return int : plus grand id photo 
     * @throws SQLException
     */
    public int idPhMax() throws SQLException {
        Statement s = this.connexion.createStatement();
        ResultSet r = s.executeQuery("SELECT ifnull(max(idPh),0) from PHOTO");
        r.next();
        return r.getInt(1);
    }

    /**
     * Méthode qui permet d'ajouter d'une vente 
     * @param prixBase int :le prix de base de l'objet 
     * @param prixMin int : le prix minimum de l'objet 
     * @param tempsAvantDebut int : le temps en heure avant le début de la vente 
     * @param duree int : la durée en heure de la vente 
     * @param categorie String : la catégorie de l'objet
     * @param nomOb String : le nom de l'objet 
     * @param describOb String : La description de l'objet 
     * @param photo Image : L'image correspondant à l'objet 
     * @param idUt int : l'identifiant de l'utilisateur qui met l'objet en vente 
     * @throws SQLException
     */
    public void ajouteVente(int prixBase, int prixMin, int tempsAvantDebut, int duree, String categorie, String nomOb, String describOb, Image photo, int idUt) throws SQLException {

        ImageView imgView = new ImageView(photo);
        imgView.setFitWidth(20);
        imgView.setFitHeight(30);
        Image image = imgView.getImage();
        
        int idOb = this.idObMax() + 1 ;
        int idCat = 1;
        if (categorie.equals("Vêtement")) {
            idCat = 1;
        }
        if (categorie.equals("Ustensile Cuisine")) {
            idCat = 2;
        }
        if (categorie.equals("Meuble")) {
            idCat = 3;
        }
        if (categorie.equals("Outil")) {
            idCat = 4;
        }
        PreparedStatement ps = this.connexion.prepareStatement("INSERT INTO OBJET values(?,?,?,?,?);");
        ps.setInt(1, idOb);
        ps.setString(2, nomOb);
        ps.setString(3,describOb);
        ps.setInt(4, idUt);
        ps.setInt(5, idCat);
        ps.executeUpdate();

        int idVe = this.idVeMax()+1;
        PreparedStatement ps2 = this.connexion.prepareStatement("INSERT INTO VENTE values(?,?,?,DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL "+tempsAvantDebut+ " HOUR),DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL "+(tempsAvantDebut+duree)+ " HOUR),?,1);");
        ps2.setInt(1,idVe);
        ps2.setInt(2, prixBase);
        ps2.setInt(3, prixMin);
        ps2.setInt(4, idOb);
        ps2.executeUpdate();

        if (photo != null) {
            System.out.println("Ok");
            int idPh = this.idPhMax()+1;
            System.out.println(this.imageToByteArray(image));
            Blob blob = new SerialBlob(this.imageToByteArray(image));
            System.out.println("Ok 2");
            PreparedStatement ps3 = this.connexion.prepareStatement("INSERT INTO PHOTO values (?,?,?,?);");
            System.out.println(idPh);
            System.out.println(idOb);
            ps3.setInt(1, idPh);
            ps3.setString(2, "Une description");
            ps3.setBlob(3, blob);
            ps3.setInt(4, idOb);
            ps3.executeUpdate();
        }
    }

    public byte[] imageToByteArray(Image image) {
        PixelReader pixelReader = image.getPixelReader();
        int width = (int) image.getWidth();
        int height = (int) image.getHeight();
    
        WritablePixelFormat<ByteBuffer> pixelFormat = PixelFormat.getByteBgraInstance();
        ByteBuffer buffer = ByteBuffer.allocate(width * height * 4);
        pixelReader.getPixels(0, 0, width, height, pixelFormat, buffer, width * 4);
    
        return buffer.array();
    }

    public void changerLePseudo(String newPseudo,int ID ) throws SQLException {
        try {
            PreparedStatement ps = this.connexion.prepareStatement("UPDATE UTILISATEUR SET pseudout = ? WHERE idut = ?");
            ps.setString(1, newPseudo);
            ps.setInt(2, ID);
            ps.executeUpdate();
        } 
        catch (SQLException e) {
            throw e;
        }
    }

    public void changerLEmail(String newEmail,int ID ) throws SQLException{
        try {
            PreparedStatement ps = this.connexion.prepareStatement("UPDATE UTILISATEUR SET emailUt = ? WHERE idut = ?");
            ps.setString(1, newEmail);
            ps.setInt(2, ID);
            ps.executeUpdate();
        } 
        catch (SQLException e) {
            throw e;
        }
    }

    public void changerLeMDP(String newMDP,int ID ) throws SQLException{
        try {
            PreparedStatement ps = this.connexion.prepareStatement("UPDATE UTILISATEUR SET mdpUt = ? WHERE idut = ?");
            ps.setString(1, newMDP);
            ps.setInt(2, ID);
            ps.executeUpdate();
        } 
        catch (SQLException e) {
            throw e;
        }
    }

    public int getIdprofil(String mail, String pseudo) throws SQLException{
        try {
            PreparedStatement ps = this.connexion.prepareStatement("SELECT idut FROM UTILISATEUR WHERE pseudout = ? AND emailut = ?;");
            ps.setString(1,pseudo);
            ps.setString(2,mail);
            ResultSet r = ps.executeQuery();
            if (r.next()) {
                return r.getInt("idut");
            } 
            else {
                return 0;
            }
        } 
        catch (SQLException e) {
            throw e;
        }
    }

    public List<String>  ListedesEmails() throws SQLException{
        try{
            Statement s = this.connexion.createStatement();
            ResultSet r = s.executeQuery("Select emailUt from UTILISATEUR;");
            List<String> lstEmail = new ArrayList<>();
            while(r.next()) {
                lstEmail.add(r.getString(1));
            }
        return lstEmail;
        } 
        catch (SQLException e) {
            throw e;
        }
    }

    public List<String>  ListedesPseudos() throws SQLException{
        try{
            Statement s = this.connexion.createStatement();
            ResultSet r = s.executeQuery("Select pseudoUt from UTILISATEUR;");
            List<String> lstPseudo = new ArrayList<>();
            while(r.next()) {
                lstPseudo.add(r.getString(1));
            }
        return lstPseudo;
        } 
        catch (SQLException e) {
            throw e;
        }
    }

    public Text acquisition30Jours(int id){
        Text nbimpressionclient = new Text("");
        try{
            PreparedStatement ps = connexion.prepareStatement("select count(idob) from VENTE natural join OBJET natural join UTILISATEUR where idut = ? and idob in (select idob from VENTE natural join STATUT where idst = 4 and DATEDIFF(CURDATE(),finve) < 30);");
            ps.setInt(1, id);
            ResultSet resultatRequete = ps.executeQuery();
            resultatRequete.next();
            int v = resultatRequete.getInt(1);
            nbimpressionclient = new Text(""+v);
        }
        catch(Exception e){}
        return nbimpressionclient;
    }

    public List<Double> catObjetAchetes(int id){
        double vTot = 0;
        double v1=0;
        double v2=0;
        double v3=0;
        double v4=0;
        try{
            PreparedStatement ps = connexion.prepareStatement("select count(idob) from VENTE natural join OBJET natural left outer join CATEGORIE natural join UTILISATEUR where idut = ? group by idcat;");
            ps.setInt(1, id);
            ResultSet resultatRequete = ps.executeQuery();
            int i = 0;
            for (int  u = 0; u < 4; u++){
                if (resultatRequete.next() != false){
                    vTot += resultatRequete.getDouble(1);
                    if (i == 0){
                        v1 = resultatRequete.getDouble(1);
                    }
                    if(i == 1){
                        v2 = resultatRequete.getDouble(1);
                    }
                    if(i == 2){
                        v3 = resultatRequete.getDouble(1);
                    }
                    if(i == 3){
                        v4 = resultatRequete.getDouble(1);
                    }
                    i+=1;
                }
            }
        }
        catch(Exception e){}
        List<Double> res = new ArrayList<>();
        res.add(v1);
        res.add(v2);
        res.add(v3);
        res.add(v4);
        res.add(vTot);
        return res;
    }

    public Text nbObjetVendusParCat(int id){
        Text nbObjetObtenuParCat = new Text("");
        try{
            PreparedStatement ps = connexion.prepareStatement("select count(idob) from VENTE natural join OBJET natural left outer join CATEGORIE natural join UTILISATEUR where idut = ? group by idcat;");
            ps.setInt(1, id);
            ResultSet resultatRequete = ps.executeQuery();
            String res = "";
            int i = 0;
            for (int  u = 0; u < 4; u++){
                if (resultatRequete.next() != false){
                    int v = resultatRequete.getInt(1);
                    if (i == 0){
                        res = res+"Vêtement : "+v+"\n";
                    }
                    if(i == 1){
                        res = res+"Ustensile Cuisine : "+v+"\n";
                    }
                    if(i == 2){
                        res = res+"Meuble : "+v+"\n";
                    }
                    if(i == 3){
                        res = res+"Outil : "+v+"\n";
                    }
                    i+=1;
                }
            }
            nbObjetObtenuParCat = new Text(res);
        }
        catch(Exception e){}
        return nbObjetObtenuParCat;
    }

    public List<Integer> dernieresAcquisition(int id){
        int v1 = 10;
        int v2 = 10;
        int v3 = 10;
        int v4 = 10;
        int v5 = 10;
        try{
            PreparedStatement ps1 = connexion.prepareStatement("select count(idob) from OBJET natural join UTILISATEUR where idut = ? and idob in (select idob from VENTE natural join STATUT where idst = 4 and DATEDIFF(CURDATE(),finve) = 1);");
            PreparedStatement ps2 = connexion.prepareStatement("select count(idob) from OBJET natural join UTILISATEUR where idut = ? and idob in (select idob from VENTE natural join STATUT where idst = 4 and DATEDIFF(CURDATE(),finve) = 2);");
            PreparedStatement ps3 = connexion.prepareStatement("select count(idob) from OBJET natural join UTILISATEUR where idut = ? and idob in (select idob from VENTE natural join STATUT where idst = 4 and DATEDIFF(CURDATE(),finve) = 3);");
            PreparedStatement ps4 = connexion.prepareStatement("select count(idob) from OBJET natural join UTILISATEUR where idut = ? and idob in (select idob from VENTE natural join STATUT where idst = 4 and DATEDIFF(CURDATE(),finve) = 4);");
            PreparedStatement ps5 = connexion.prepareStatement("select count(idob) from OBJET natural join UTILISATEUR where idut = ? and idob in (select idob from VENTE natural join STATUT where idst = 4 and DATEDIFF(CURDATE(),finve) = 5);");
            ps1.setInt(1, id);
            ps2.setInt(1, id);
            ps3.setInt(1, id);
            ps4.setInt(1, id);
            ps5.setInt(1, id);
            ResultSet resultatRequete1 = ps1.executeQuery();
            ResultSet resultatRequete2 = ps2.executeQuery();
            ResultSet resultatRequete3 = ps3.executeQuery();
            ResultSet resultatRequete4 = ps4.executeQuery();
            ResultSet resultatRequete5 = ps5.executeQuery();
            resultatRequete1.next();
            resultatRequete2.next();
            resultatRequete3.next();
            resultatRequete4.next();
            resultatRequete5.next();
            v1 = resultatRequete1.getInt(1);
            v2 = resultatRequete2.getInt(1);
            v3 = resultatRequete3.getInt(1);
            v4 = resultatRequete4.getInt(1);
            v5 = resultatRequete5.getInt(1);
        }
        catch(Exception e){}
        List<Integer> res = new ArrayList<>();
        res.add(v1);
        res.add(v2);
        res.add(v3);
        res.add(v4);
        res.add(v5);
        return res;
    }

    /**
    * Méthode qui permet de rechercher un objet à partir de la barre de recherche
    * @param recherche String : la recherche de l'utilisateur
    * @return ArrayList<Produit> : la liste des objets correspondant à la recherche
    */
    public ArrayList<Produit> rechercheProduits(String recherche) throws SQLException {
        //Recuperation information produits, utilisateurs et encherissements
        PreparedStatement ps = this.connexion.prepareStatement("select distinct idut, pseudout, emailut, mdput, activeut, idOb, nomob, descriptionob, montant, nomcat from UTILISATEUR natural join OBJET natural left join ENCHERIR natural left join CATEGORIE WHERE nomob LIKE ? OR descriptionob LIKE ?;");
        ps.setString(1, "%"+recherche+"%");
        ps.setString(2, "%"+recherche+"%");
        ResultSet r = ps.executeQuery();
        ArrayList<Produit> listeProduits = new ArrayList<Produit>();
        r.next();
        while (r.next()) {
            boolean active;
            if (r.getString(5).charAt(0) == 'O'){
                active = true;
            }
            else{
                active = false;
            }
            Utilisateur u = new Utilisateur(r.getInt(1), r.getString(2), r.getString(3), r.getString(4), active);
            //Recuperation de toutes les encheres pour un produit
            PreparedStatement ps2 = this.connexion.prepareStatement("SELECT distinct montant FROM ENCHERIR NATURAL right JOIN UTILISATEUR NATURAL right JOIN OBJET WHERE idob = ?;");
            ps2.setInt(1, r.getInt(6));
            ResultSet r2 = ps2.executeQuery();
            r2.next();
            ArrayList<Double> listeEncheres = new ArrayList<>();
            r2.next();
            while (r2.next()) {
                listeEncheres.add(r2.getDouble(1));
            }
            if (listeEncheres.isEmpty()) {
                listeEncheres.add(0.0);
            }
            double maxenchere = Collections.max(listeEncheres);
            Produit pdt = new Produit(r.getInt(6), r.getString(7), r.getString(8), new Image("./Loupe.png")//r.getBlob(8)
            , u, maxenchere, listeEncheres,r.getString(10));
            Produit pAModif = null;
            if (listeProduits.isEmpty()){
                listeProduits.add(pdt);
            }
            for (Produit p : listeProduits){
                if (pdt.getNom().equals(p.getNom())){
                    pAModif = p;
                    break;
                }
            }
            if (pAModif != null && pdt.getDernierPrix() > pAModif.getDernierPrix()){
                listeProduits.remove(pAModif);
                listeProduits.add(pdt);
            }
            else if (pAModif == null){
                listeProduits.add(pdt);
            }
        }
        System.out.println(listeProduits);
        return listeProduits;
    }
    public int getmaxmontant(int idOb) throws SQLException{
        try {
        PreparedStatement statement = connexion.prepareStatement("select max(montant) from ENCHERIR natural join VENTE where idOb = ?;");
        statement.setInt(1, idOb);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    return 0;
    }
}