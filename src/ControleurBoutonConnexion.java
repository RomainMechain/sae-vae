import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import java.sql.SQLException;

public class ControleurBoutonConnexion implements EventHandler<ActionEvent> {

    private VueVAE appli;

    public ControleurBoutonConnexion(VueVAE appli) {
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent a) {
        try {
            this.appli.setUser(this.appli.getEncherirBD().seConnecter(this.appli.gettMail().getText(),this.appli.gettPassword().getText()));
        }
        catch (SQLException e ) {
            System.out.println("Erreur");
        }
        catch (ExceptionMailNonPresent e) {
            this.appli.popUpMessageEmail().showAndWait();
        }
        catch (ExceptionMdpIncorect e) {
            this.appli.popUpMessageMdpIncorrect().showAndWait();
        }
        catch (ExceptionCompteDesactive e) {
            System.out.println("Le compte est désactivé");
        }
        if (this.appli.getUser() != null) {
            this.appli.modeVente();
        }
    }
}
