import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

public class ControlleurBoutonEnchere implements EventHandler<ActionEvent> {

    /**
     * La vue de l'application
     */
    private VueVAE appli;
    /**
     * prix de l'enchère que l'on souhaite mettre pour le produit
     */
    private String prixStr;
    /**
     * le produit sur lequel on soihaite enchérir
     */
    private Produit produit;

    /**
     * 
     * @param app La vue de l'application
     * @param produit le produit sur lequel on veut enchérir
     */
    public ControlleurBoutonEnchere(VueVAE app, Produit produit) {
        this.appli = app;
        this.produit = produit;
    }

    /**
     * lorsque l'on propose une enchère en appuyant sur le bouton adéquat, on récupère ici les données nécessaires à l'évaluation de cette nouvelle enchère, et si elle corespond à ce que l'on attend, le visuel de l'enchère est mis à jour
     * @param a lorrsque le bouton est pressé
     */
    @Override
    public void handle(ActionEvent a) {
       this.prixStr = this.appli.getValeurEnchere(produit);
        try {
            System.out.println(prixStr);
            double prix = Double.parseDouble(this.prixStr);
            this.produit.nouvelleEnchere(prix);
            this.appli.afficheFicheProduit(this.produit);
            this.appli.miseAJourVisuel();
        } catch (PrixNonValideException e) { // si le prix proposé est inférieur à celui déjà présent
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Erreur de prix");
            alert.setContentText("Le prix proposé est inférieur au dernier prix proposé");
            alert.showAndWait(); // apparition de la pop up
        } catch (NumberFormatException e) { // si le prix proposé n'est pas uniquement composé de chiffre 
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Erreur de prix");
            alert.setContentText("Le prix proposé n'est pas un nombre");
            alert.showAndWait(); // apparition de la pop up
        }
    }
}
