import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurRetourConnexion implements EventHandler<ActionEvent> {
    /**
     * La vue de l'application
     */
    private VueVAE vue;

    /**
     * le controleur permet de revenir sur la page de connexion de l'application lorsque l'on était sur la page d'inscription
     * @param vue La vue de l'application
     */
    public ControleurRetourConnexion(VueVAE vue) {
        this.vue = vue;
    }

    /**
     * bascule la vue sur la page de connexion en appellant la fonction adéquate
     * @param e lorsque le bouton est pressé
     */
    @Override
    public void handle(ActionEvent e) {
        this.vue.modeConnexion();
    }
    
}
