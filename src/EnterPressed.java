import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import java.sql.SQLException;

public class EnterPressed implements EventHandler<KeyEvent> {

    private VueVAE appli;

    public EnterPressed(VueVAE appli) {
        this.appli = appli;
    }

    @Override
    public void handle(KeyEvent a) {
        if (a.getCode().equals(KeyCode.ENTER)) {
            try {
                this.appli.setUser(this.appli.getEncherirBD().seConnecter(this.appli.gettMail().getText(),this.appli.gettPassword().getText()));
            }
            catch (SQLException e ) {
                System.out.println("Erreur");
            }
            catch (ExceptionMailNonPresent e) {
                this.appli.popUpMessageEmail().showAndWait();
            }
            catch (ExceptionMdpIncorect e) {
                this.appli.popUpMessageMdpIncorrect().showAndWait();
            }
            catch (ExceptionCompteDesactive e) {
                System.out.println("Compte desac");
            }
            if (this.appli.getUser() != null) {
                this.appli.modeVente();
            }
        }
    }
}
