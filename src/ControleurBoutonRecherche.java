
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurBoutonRecherche implements EventHandler<ActionEvent>{
    /**
     * La vue de l'application
     */
    private VueVAE vue;

    /**
     * initialise le bouton controleur qui va rechercher grâce au résultat de la barre de recherche  
     * @param vue la vue de l'application
     */
    public ControleurBoutonRecherche(VueVAE vue) {
        this.vue = vue;
    }

    /**
     * permet de rechercher les article correspondant le plus dans la base de données à ce qui est présent dans la barre de recherche
     * @param event une fois que le bouton est pressé
     */
    @Override
    public void handle(ActionEvent event) {
        if(event.getTarget().equals(vue.getRecherche())){
        System.out.println(this.vue.getRecherche().getText());
        }
        else if(event.getTarget().equals(vue.getboutonmenu())){
        }
        
    }
}
