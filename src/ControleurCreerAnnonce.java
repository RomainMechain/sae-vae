import javafx.scene.image.Image;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurCreerAnnonce implements EventHandler<ActionEvent> {
    
    /**
     * La vue de l'application
     */
    private VueVAE vueApp;

    /**
     * crée le controleur qui va mettre l'annonce rédigé auparavant dans la base de données
     * @param vueApp La vue de l'application
     */
    public ControleurCreerAnnonce(VueVAE vueApp) {
        this.vueApp = vueApp;
    }

    /**
     * va créé une nouvelle annonces, rentrer ces données dans la base de données, mettre à jour le visuel et rajoute à gauche dans la page la nouvelle annonce
     * @param actionEvent une fois que le bouton est pressé
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        try {
            int prixBase = this.vueApp.getPrixObjetAVendre();
            int prixMin = this.vueApp.getPrixMinObjetAVendre();
            int tpsAvant = this.vueApp.getTempsAvantMiseEnVente();
            int durer = this.vueApp.getDureeVente();
            String categorie = this.vueApp.getCateObjetAVendre();
            String nomOb = this.vueApp.getNomObjetAVendre();
            String descOb = this.vueApp.getDescriptionObjetAVendre();
            Image photo = this.vueApp.getImageObjetAVendre().getImage();
            int idUt = this.vueApp.getIdUtilisateur();
            this.vueApp.getEncherirBD().ajouteVente(prixBase, prixMin, tpsAvant, durer, categorie, nomOb, descOb, photo, idUt);
            this.vueApp.modeVente();
        }
        catch (Exception e) {
            this.vueApp.popUpMessageNombre().showAndWait();
        }
        
    }

}
