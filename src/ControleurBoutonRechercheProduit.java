import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import java.util.ArrayList;
import java.sql.SQLException;

public class ControleurBoutonRechercheProduit implements EventHandler<ActionEvent>{
    
    private VueVAE vue;
    private EncherirBD encherirBD;

    public ControleurBoutonRechercheProduit(VueVAE vue, EncherirBD encherirBD) {
        this.vue = vue;
        this.encherirBD = encherirBD;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        // on récupère le texte de la barre de recherche
        String recherche = vue.getRecherche().getText();
        // on récupère la liste des objets correspondant à la recherche
        ArrayList<Produit> lstProduits = null;
        try{
            lstProduits = encherirBD.rechercheProduits(recherche);
        }
        catch (SQLException ex){
            System.out.println("Erreur SQL");
        }
        // on affiche les objets dans la liste
        vue.setProduits(lstProduits);
        vue.affichePageOffre();
    }
}
