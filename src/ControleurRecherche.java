import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import java.util.ArrayList;
import java.sql.SQLException;

public class ControleurRecherche implements EventHandler<KeyEvent>{
    
    private VueVAE vue;

    private EncherirBD encherirBD;

    public ControleurRecherche(VueVAE vue, EncherirBD encherirBD) {
        this.vue = vue;
        this.encherirBD = encherirBD;
    }

    @Override
    public void handle(KeyEvent e) {
        // si la touche préssée est espace
        if (e.getCode().equals(KeyCode.ENTER)){
            // on récupère le texte de la barre de recherche
            String recherche = vue.getRecherche().getText();
            // on récupère la liste des objets correspondant à la recherche
            ArrayList<Produit> lstProduits = null;
            try{
                lstProduits = encherirBD.rechercheProduits(recherche);
            }
            catch (SQLException ex){
                System.out.println("Erreur SQL");
            }
            // on affiche les objets dans la liste
            vue.setProduits(lstProduits);
            vue.affichePageOffre();
        }
    }
}
