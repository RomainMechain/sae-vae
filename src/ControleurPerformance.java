import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurPerformance implements EventHandler<ActionEvent> {
    /**
     * La vue de l'application
     */
    private VueVAE vue;
    
    /**
     * permet de changer de page pour arriver sur la page des performances
     * @param vue La vue de l'application
     */
    public ControleurPerformance(VueVAE vue) {
        this.vue = vue;
    }
    
    /**
     * appelle une fonction qui va changer la page en celle qui est défini par la fonction, donc la page de performance
     * @param event lorsque le bouton est pressé
     */
    @Override
    public void handle(ActionEvent event) {
        vue.pagePerformance();
    }
}
