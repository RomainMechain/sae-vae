import java.util.List;
import javafx.scene.image.Image;
public class Produit{
    private String nom;
    private String description;
    private Image images;
    private Utilisateur user;
    private Double dernierPrix;
    private List<Double> listeEncheres;
    private String categorie;
    private int idOb;

    public Produit(int idOb, String nom, String description, Image image, Utilisateur user, double dernierPrix, List<Double> listeEncheres,String categorie) {
        this.nom = nom;
        this.description = description;
        this.images = image;
        this.user = user;
        this.dernierPrix = dernierPrix;
        this.listeEncheres = listeEncheres;
        this.categorie = categorie;
        this.idOb = idOb;
    }

    public String getNom() {
        return this.nom;
    }

    public String getDescription() {
        return this.description;
    }

    public Image getImages() {
        return this.images;
    }

    public Utilisateur getUser() {
        return this.user;
    }

    public Double getDernierPrix() {
        return this.dernierPrix;
    }

    public List<Double> getListeEncheres() {
        return this.listeEncheres;
    }

    public void nouvelleEnchere(double prix) throws PrixNonValideException {
        if (prix < this.dernierPrix) {
            throw new PrixNonValideException();
        }
        this.dernierPrix = prix;
        this.listeEncheres.add(prix);
    }

    public void chargerImage(String image){
        this.images = new Image(image);
    }
    public String getCategorie(){
        return this.categorie;
    }

    @Override
    public boolean equals(Object o){
        if (o == null){
            return false;
        }
        if (o == this){
            return true;
        }
        if (o instanceof Produit){
            Produit p = (Produit) o;
            return this.nom.equals(p.getNom()) &&
            this.description.equals(p.getDescription()) &&
            this.images.equals(p.getImages()) &&
            this.user.equals(p.getUser()) &&
            this.dernierPrix.equals(p.getDernierPrix()) &&
            this.categorie.equals(p.getCategorie()) &&
            this.listeEncheres.equals(p.getListeEncheres());
        }
        return false;
    }
}