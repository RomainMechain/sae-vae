import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurProduit implements EventHandler<ActionEvent> {
    /**
     * La vue de l'application
     */
    private VueVAE vue;

    /**
     * permet de changer de page pour arriver sur la page des produits
     * @param vue La vue de l'application
     */
    public ControleurProduit(VueVAE vue) {
        this.vue = vue;
    }

    /**
     * appelle une fonction qui va changer la page en celle qui est défini par la fonction, donc la page des produits
     * @param a lorsque le bouton est pressé
     */
    @Override
    public void handle(ActionEvent a) {
        vue.affichePageOffre();
    }
}
