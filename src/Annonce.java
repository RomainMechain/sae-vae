import javafx.scene.image.Image;

public class Annonce {
    private String nom;
    private String description;
    private Image img;
    private int prix;

    public Annonce(String nom, String description, Image img, int prix) {
        this.nom = nom;
        this.description = description;
        this.img = img;
        this.prix = prix;
    }
    public String getNom() {
        return this.nom;
    }
    public String getDescription() {
        return this.description;
    }
    public Image getImg() {
        return this.img;
    }
    public int getPrix() {
        return this.prix;
    }


}
