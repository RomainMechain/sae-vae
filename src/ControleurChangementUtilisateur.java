import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurChangementUtilisateur implements EventHandler<ActionEvent> {

    private VueVAE appli;
    private String nombouton;

    public ControleurChangementUtilisateur(VueVAE appli, String nombouton) {
        this.appli = appli;
        this.nombouton = nombouton;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        if (this.nombouton.equals("Pseudo")){
            this.appli.popUpNewPseudo().showAndWait();
            this.appli.changePseudo();
        }
        else if (this.nombouton.equals("Email")){
            this.appli.popUpNewEmail().showAndWait();
            this.appli.changeEmail();
        }
        else if (this.nombouton.equals("MDP")){
            this.appli.popUpNewMDP().showAndWait();
            this.appli.changeMDP();
        }
        else {
            this.appli.popUpMessageErreur();
        }
    }
}


