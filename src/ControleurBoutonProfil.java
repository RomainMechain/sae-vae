import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurBoutonProfil implements EventHandler<ActionEvent> {

    private VueVAE appli;

    public ControleurBoutonProfil(VueVAE appli) {
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.appli.modeProfilUtilisateur();
    }
}


