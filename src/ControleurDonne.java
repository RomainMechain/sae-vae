import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.sql.SQLException;

public class ControleurDonne implements EventHandler<ActionEvent> {

    /**
     * La vue de l'application
     */
    private VueVAE appli;

    /**
     * Va permettre l'inscription d'un nouvel utilisateur de la plateforme
     * @param appli La vue de l'application
     */
    public ControleurDonne(VueVAE appli) {
        this.appli = appli;
    }

    /**
     * ce controleur va inscrire le nouvel utilisateur dans la base de données si ces dernières sont valides
     * @param actionEvent une fois que le bouton est pressé
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        if (this.appli.getPseudo().getText().isEmpty() || this.appli.getEmail().getText().isEmpty() || this.appli.getMdp1().getText().isEmpty() || this.appli.getMdp2().getText().isEmpty()){ // si un des champs n'est pas remplit
            this.appli.popUpMessageChampsVide().showAndWait();
        }
        else{
            if (VueVAE.isValidEmail(this.appli.getEmail().getText())){ // si l'adresse mail rentré est valide
                if (this.appli.getMdp1().getText().equals(this.appli.getMdp2().getText())){ // si les deux mot de passe rentrés sont identiques
                    if (VueVAE.isPasswordComplex(this.appli.getMdp1().getText())){ // si le mot de passe est pas assez élaboré
                        System.out.println("" + this.appli.pseudo.getText());
                        System.out.println("" + this.appli.getEmail().getText());
                        //System.out.println("" + this.appli.cryptage(this.appli.getMdp1().getText()));
                        //System.out.println("" + this.appli.decryptage(this.appli.cryptage(this.appli.getMdp1().getText())));
                        System.out.println("" + this.appli.getMdp1().getText());
                        System.out.println("" + this.appli.getMdp2().getText());
                        try {
                            int idUt = this.appli.getEncherirBD().idUtMax();
                            this.appli.getEncherirBD().ajouteUtilisateur(idUt+1, this.appli.pseudo.getText(), this.appli.getEmail().getText(), this.appli.getMdp1().getText());
                            this.appli.setUser(this.appli.getEncherirBD().seConnecter(this.appli.getEmail().getText(), this.appli.getMdp1().getText()));
                            this.appli.modeVente();
                        }
                        catch (SQLException e ) {
                            System.out.println("erreur SQLException");
                        }
                        catch (ExceptionMailNonPresent e) {
                            System.out.println("erreur ExceptionMailNonPresent");
                        }
                        catch (ExceptionMdpIncorect e ) {
                            System.out.println("erreur ExceptionMdpIncorect");
                        }
                        catch (ExceptionCompteDesactive e ) {
                            System.out.println("erreur ExceptionCompteDesactive");
                        }
                    }
                    else{
                        this.appli.popUpMessageMdpPasComplex().showAndWait();
                    }
                }
                else{
                    this.appli.popUpMessageMdpDiff().showAndWait();
                }
            }
            else{
                this.appli.popUpMessageEmail().showAndWait();
            }
        }
        
    }
}


